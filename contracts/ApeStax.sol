// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./interfaces/IUniswapV2Pair.sol";
import "./libararies/UQ112x112.sol";
import "./interfaces/IUniswapV2Router02.sol";

contract ApeStax is ERC20, ERC20Burnable, Pausable, Ownable {
    using UQ112x112 for uint224;

    bool public SELL_ONE;
    bool public SELL_TWO;
    bool public SELL_THREE;

    uint256 public rate;
    uint256 public sell_ends;

    address public LOTTERY; // lottery pool address
    address public LP; // liquidity pool address
    address public R_N_D; // marketing and development address

    address public liquidity_v2_pool = 0xa32Ae418a09C14E9B7FEe1c286c974D51f849F51;
    address public router = 0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D;

    constructor() ERC20("ApeStax", "$ASTAX \xF0\x9F\x90\xB5") {
        sell_ends = block.timestamp + 24 hours;
        rate = 500; // 500 per ethereum
    }

    function pause() public onlyOwner {
        _pause();
    }

    function unpause() public onlyOwner {
        _unpause();
    }

    function setLottery(address _lottery) public onlyOwner {
        LOTTERY = _lottery;
    }

    function setlLiquidityPool(address _lp) public onlyOwner {
        LP = _lp;
    }

    function setRND(address _rnd) public onlyOwner {
        R_N_D = _rnd;
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal override whenNotPaused {
        super._beforeTokenTransfer(from, to, amount);
    }

    // Fallback function
    // @dev restrict users not send ethers
    fallback() external {
        revert("Please use buy function");
    }

    // payable function
    // @dev restrict users not send ethers
    receive() external payable {
        revert("Please use buy function");
    }

    // price impact for buying {tokens} amount of tokens
    function computePriceImpact(uint256 _amountIn, address tokenIn)
        external
        view
        returns (uint256)
    {
        require(_amountIn > 0);
        // fetch token reserves from liquidity pool
        IUniswapV2Pair pool = IUniswapV2Pair(liquidity_v2_pool);
        IUniswapV2Router02 _router = IUniswapV2Router02(router);
        address tokenA = pool.token0();
        (uint256 _reserveA, uint256 _reserveB, ) = pool.getReserves();
        uint256 reserveIn = tokenA != tokenIn ? _reserveA : _reserveB;
        uint256 reserveOut = tokenA == tokenIn ? _reserveA : _reserveB;
        uint256 amountIn =
            _router.getAmountOut(_amountIn, reserveIn, reserveOut);

        uint256 price_impact = (((amountIn * 997) / 1000) *1000 )/ reserveOut; // compute 97% of tokens
        return price_impact;
    }

    function reserves() external view returns (uint256, uint256) {
        IUniswapV2Pair pool = IUniswapV2Pair(liquidity_v2_pool);
        (uint256 _reserveA, uint256 _reserveB, ) = pool.getReserves();
        return (_reserveA, _reserveB);
    }

    function buy() external payable {
        require(msg.value > 0);
        updateTimestamp();
        updateSellCategory();
        uint256 tokens = msg.value * rate;
        if (SELL_ONE) distributionToFirstSell(tokens);
        if (SELL_TWO) distributionToSecondSell(tokens);
        if (SELL_THREE) distributionToThirdSell(tokens);
        // calculate price impact := (exactQuote - outputAmount) / exactQuote
        // const quotedOutputAmount = midPrice.quote(inputAmount)
        //const priceImpact = quotedOutputAmount.subtract(outputAmount).divide(quotedOutputAmount)
        // uint priceImpact =
    }

    function updateTimestamp() internal {
        if (block.timestamp > sell_ends) {
            sell_ends = block.timestamp + 24 hours;
            SELL_ONE = false;
            SELL_TWO = false;
            SELL_THREE = false;
        }
    }

    function updateSellCategory() internal {
        // check for first sell
        if (!SELL_ONE && !SELL_TWO && !SELL_THREE)
            SELL_ONE = true;
            // check for second sell and update second sell flag, turn off all other sales flags
            // so only second sell remains on.
        else if (SELL_ONE && !SELL_TWO && !SELL_THREE) {
            SELL_TWO = true;
            SELL_ONE = false;
        }
        // check for third sell and update third sell flag, turn off all other sales flags
        // so only third sell remains on.
        else if (!SELL_ONE && SELL_TWO && !SELL_THREE) {
            SELL_THREE = true;
            SELL_TWO = false;
        }
    }

    // distribute 13 %
    function distributionToFirstSell(uint256 tokens) internal {
        // Calculate 2% as a 200 in basis point(parts per 10,000);
        uint256 lottery_pool = (tokens * 200) / 10000;

        // Calculate 2% as a 200 in basis point(parts per 10,000);
        uint256 liquidity_pool = (tokens * 200) / 10000;

        // Calculate 8% as a 800 in basis point(parts per 10,000);
        uint256 market_development = (tokens * 800) / 10000;

        // Calculate 1% as a 100 in basis point(parts per 10,000);
        uint256 burn_tokens = (tokens * 100) / 10000;

        super._mint(LOTTERY, lottery_pool);
        super._mint(LP, liquidity_pool);
        super._mint(R_N_D, market_development);
        emit Transfer(msg.sender, address(0), burn_tokens);
    }

    // distribute 25 %
    function distributionToSecondSell(uint256 tokens) internal {
        // Calculate 5% as a 500 in basis point(parts per 10,000);
        uint256 lottery_pool = (tokens * 500) / 10000;

        // Calculate 4% as a 400 in basis point(parts per 10,000);
        uint256 liquidity_pool = (tokens * 400) / 10000;

        // Calculate 14% as a 1400 in basis point(parts per 10,000);
        uint256 market_development = (tokens * 1400) / 10000;

        // Calculate 2% as a 200 in basis point(parts per 10,000);
        uint256 burn_tokens = (tokens * 200) / 10000;

        super._mint(LOTTERY, lottery_pool);
        super._mint(LP, liquidity_pool);
        super._mint(R_N_D, market_development);
        emit Transfer(msg.sender, address(0), burn_tokens);
    }

    // distribute 35 %
    function distributionToThirdSell(uint256 tokens) internal {
        // Calculate 4% as a 400 in basis point(parts per 10,000);
        uint256 lottery_pool = (tokens * 400) / 10000;

        // Calculate 9% as a 900 in basis point(parts per 10,000);
        uint256 liquidity_pool = (tokens * 900) / 10000;

        // Calculate 19% as a 1900 in basis point(parts per 10,000);
        uint256 market_development = (tokens * 1900) / 10000;

        // Calculate 3% as a 300 in basis point(parts per 10,000);
        uint256 burn_tokens = (tokens * 300) / 10000;

        super._mint(LOTTERY, lottery_pool);
        super._mint(LP, liquidity_pool);
        super._mint(R_N_D, market_development);
        emit Transfer(msg.sender, address(0), burn_tokens);
    }
}
