const ApeStax = artifacts.require("ApeStax");
require('dotenv').config();

module.exports = function (deployer, network, accounts) {
    let deployAgain = (process.env.DEPLOY_AGAIN === 'true') ? true : false;
    
    console.log(network);
    console.log(accounts);

    // let teamAddress = '0x2513B249B1b46A3BfC6df800F28594A0B13dDF54';
    // let marketingFundsAddress = '0x2513B249B1b46A3BfC6df800F28594A0B13dDF54';
    // let developmentFundsAddress = '0x2513B249B1b46A3BfC6df800F28594A0B13dDF54';

    deployer.then(async () => {
    //    await deployer.deploy(ApeStax,teamAddress,marketingFundsAddress,developmentFundsAddress);
       await deployer.deploy(ApeStax);
    });
};