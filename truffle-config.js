const path = require("path");

const dotenv = require('dotenv').config();
const HDWalletProvider = require('@truffle/hdwallet-provider');

const mnemonic = process.env.ETHEREUM_ACCOUNT_MNEMONIC;
const privateKey = process.env.ETHEREUM_ACCOUNT_PRIVATE_KEY;

module.exports = {
    api_keys: {
        etherscan: process.env.ETHERSCAN_API_KEY
    },
    plugins: [
        'truffle-plugin-verify',
        '@chainsafe/truffle-plugin-abigen'
    ],
    networks: {
        // mainnet: {
        //     provider: function() {
        //         return new HDWalletProvider(mnemonic, process.env.ALCHEMY_NODE);
        //     },
        //     network_id: '1',
        //     gasPrice: 45100000000, // 8.1 gwei
        //     skipDryRun: true,
        // },
        development: {
            host: "127.0.0.1",
            port: 7545,
            network_id: "5777" // Match any network id
        },
        kovan: {
            provider: function () {
                return new HDWalletProvider(mnemonic, process.env.KOVAN_INFURA_ENDPOINT, 0, 15);
            },
            network_id: '42',
            gas: 8000000,
            gasPrice: 3000000000,
            from: '0x2513B249B1b46A3BfC6df800F28594A0B13dDF54',
            // skipDryRun: true,
        },
    },
    compilers: {
        solc: {
            version: "0.8.5",
            settings: {
                optimizer: {
                    enabled: true,
                    runs: 200
                },
            }
        }
    }
}
